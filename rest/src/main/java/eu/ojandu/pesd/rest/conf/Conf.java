package eu.ojandu.pesd.rest.conf;

import eu.ojandu.pesd.common.AMQP;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Conf {

    @Bean
    public AMQP amqp() {
        return new AMQP("rmq");
    }
}
