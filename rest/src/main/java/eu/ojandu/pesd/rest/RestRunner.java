package eu.ojandu.pesd.rest;

import eu.ojandu.pesd.common.AMQP;
import eu.ojandu.pesd.common.MessageSender;
import eu.ojandu.pesd.common.MongoService;
import eu.ojandu.pesd.common.Utils;
import eu.ojandu.pesd.common.dto.Filter;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@ComponentScan("eu.ojandu.pesd")
@EnableAutoConfiguration
@EnableTransactionManagement(proxyTargetClass = true)
@EnableJpaRepositories(basePackages = "eu.ojandu.pesd.common.repo")
@EntityScan(basePackages = "eu.ojandu.pesd.common.entity")
public class RestRunner {

    private static String host = "localhost";

    public static void main(String[] args) throws Exception {
        if(args.length > 0) {
            host = args[0];
        }

        SpringApplication.run(RestRunner.class, args);
    }


    @Bean
    public AMQP amqp() {
        return new AMQP(host);
    }

    @Bean
    public MongoService mongoService() {
        return new MongoService();
    }

    @Bean
    public MessageSender<Filter> messageSender() {
        return amqp().buildSender(Utils.FILTER_QUEUE, Filter.class);
    }

}