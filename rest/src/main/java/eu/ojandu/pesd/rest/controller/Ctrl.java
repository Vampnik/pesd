package eu.ojandu.pesd.rest.controller;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import eu.ojandu.pesd.common.dto.Filter;
import eu.ojandu.pesd.common.dto.Statistics;
import eu.ojandu.pesd.common.entity.Character;
import eu.ojandu.pesd.rest.ExceptionThrowingSupplier;
import eu.ojandu.pesd.rest.service.Serv;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class Ctrl {

    private static final Logger log = LoggerFactory.getLogger(Ctrl.class);

    private String kibanaReq;

    public Ctrl() throws IOException {
        kibanaReq = IOUtils.toString(Ctrl.class.getResourceAsStream("/kibanaTemplate.json"));
    }

    private String kibanaHost = "kibana";

    @Autowired
    private Serv serv;

    @RequestMapping(value = "/api/filter", method = RequestMethod.POST)
    public void setFilter(@RequestBody Filter filter) {
        wrap(() -> {
            serv.setFilter(filter);
            return "";
        });
    }

    @RequestMapping(value = "/api/stat", method = RequestMethod.GET)
    public @ResponseBody Statistics getStat() {
        return wrap(() -> serv.getStatistics());
    }

    @RequestMapping(value = "/api/chars", method = RequestMethod.GET)
    public @ResponseBody List<Character> getCharacters() {
        return wrap(() -> serv.getCharacaters());
    }

    private volatile String logCache = null;
    private long keepCache = 10000;
    private volatile boolean gettingLogs = false;
    private volatile long lastReqest = -1;

    @RequestMapping(value = "/api/logs", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody String getLogs() throws UnirestException {
        return wrap(() -> {
            if(System.currentTimeMillis() > lastReqest + keepCache && !gettingLogs) {
                try {
                    gettingLogs = true;
                    long end = System.currentTimeMillis();
                    long start = end - (15*60*1000);

                    String query = kibanaReq.replaceAll("\\$to", "" + end).replaceAll("\\$from", "" + start);
                    logCache =  Unirest.post("http://" + kibanaHost + ":5601/elasticsearch/_msearch?timeout=0&ignore_unavailable=true'")
                            .header("kbn-version", "4.3.1")
                            .body(query)
                            .asJson().getBody().toString();
                    lastReqest = System.currentTimeMillis();
                } finally {
                    gettingLogs = false;
                }
            }
            return logCache;
        });
    }

    private <R> R wrap(ExceptionThrowingSupplier<R> supplier) {
        try {
            return supplier.get();
        } catch (Exception e) {
            log.error("Unexpected error: ", e);
            throw new IllegalStateException(e);
        }
    }
}
