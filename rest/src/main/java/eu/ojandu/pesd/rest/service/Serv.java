package eu.ojandu.pesd.rest.service;

import eu.ojandu.pesd.common.MessageSender;
import eu.ojandu.pesd.common.MongoService;
import eu.ojandu.pesd.common.Utils;
import eu.ojandu.pesd.common.dto.Filter;
import eu.ojandu.pesd.common.dto.Statistics;
import eu.ojandu.pesd.common.entity.Character;
import eu.ojandu.pesd.common.repo.CharacterRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class Serv {

    @Autowired
    private MessageSender<Filter> filterSender;

    @Autowired
    private MongoService mongoService;

    @Autowired
    private CharacterRepo characterRepo;

    public void setFilter(Filter filter) {
        filterSender.send(filter);
    }

    public Statistics getStatistics() {
        Optional<Statistics> opt = mongoService.find(Utils.MONGO_STAT_COLLECTION, "{}", Statistics.class).stream().findFirst();
        Statistics stat;
        if(opt.isPresent()) {
            stat = opt.get();
            stat._id = null;
        } else {
            stat = new Statistics();
        }
        return stat;
    }

    public List<Character> getCharacaters() {
        List<Character> result = new LinkedList<>();
        for(Character c : characterRepo.findAll()) {
            result.add(c);
        }
        return result;
    }

}
