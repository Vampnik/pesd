package eu.ojandu.pesd.rest;

@FunctionalInterface
public interface ExceptionThrowingSupplier<T> {
    T get() throws Exception;
}
