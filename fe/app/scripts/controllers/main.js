'use strict';

/**
 * @ngdoc function
 * @name feApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the feApp
 */
angular.module('feApp')
    .controller('MainCtrl', function ($http) {
        this.awesomeThings = [
            'HTML5 Boilerplate',
            'AngularJS',
            'Karma'
        ];
        var ctrl = this;

        var raw = "abcdefghijklmopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        ctrl.ranCol = raw.split("");
        ctrl.ranLen = ctrl.ranCol.length;

        ctrl.total = 0;
        ctrl.filtered = 0;

        ctrl.sendRandomFilter = function () {
            var body = {
                filterChars: ""
            };
            for(var i=0;i<ctrl.ranLen;i++) {
                if(Math.random() < 0.05) {
                    body.filterChars = body.filterChars + ctrl.ranCol[i];
                }
            }
            body.lessThan = Math.floor((Math.random() * 1000000000) + 1);
            console.log("Sending filter: ", body);
            $http.post("api/filter", body).success(function(resp) {
                console.log("Success on sending filter", body, resp);
            }).error(function(resp) {
                console.log("Fail on sending filter", body, resp);
            });
        };

        $http.get("api/stat").success(function(resp) {
            ctrl.total = resp.total;
            ctrl.filtered = resp.filtered;
        }).error(function(resp) {
            console.log("Getting statistics failed", resp);
        });
    });
