# Partitioning Enterprise System in Docker

## User Guide

### Prerequisites 

* Docker (must be able to run docker client as non-superuser) : https://docs.docker.com/engine/installation/
* Docker Machine: https://docs.docker.com/machine/install-machine/
* JDK: http://www.oracle.com/technetwork/java/javase/downloads/index.html
* Maven: https://maven.apache.org/download.cgi
* SSH daemon running on local machine

### Steps

1) Run:
```
mvn clean install
```
2) Copy "sample-access.properties" to "access.properties"
3) Fill in "access.properties"

* local.user: local machine username
* local.pwd: local machine password
* remote.host: localhost by default (meant otherwise for remote testing)
* remote.user: same as local.user (meant otherwise for remote testing)
* remote.pwd: same as local.pwd (meant otherwise for remote testing)

4) Copy "sample-start-swarm.sh" to "start-swarm.sh"
5) Alter "start-swarm.sh" with Digital Ocean API access token
6) Run:
```
./start-swarm.sh
```
7) Run (this should open an CLI):
```
java -jar manager/target/pesd-manager-1.0-SNAPSHOT-jar-with-dependencies.jar
```
8) Viable commands:

* start-local (note that following ports must be free for this 80, 4560, 5432, 5601, 5671, 5672, 8080, 8081, 8082, 9200, 15672, 27017)
* measure-local
* part-local [numParts] [partName]
* start-swarm [random[numParts] or partName]
* measure-swarm
* part-swarm [numParts] [partName]


### Load Testing

1) Alter /etc/hosts so that hostname "jmtest" maps to host of the Nginx container (localhost for local solution)
2) Run:
```
java -cp manager/target/pesd-manager-1.0-SNAPSHOT-jar-with-dependencies.jar eu.ojandu.pesd.manager.LoadTest
```