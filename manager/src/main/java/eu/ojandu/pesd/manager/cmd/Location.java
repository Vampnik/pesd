package eu.ojandu.pesd.manager.cmd;

public abstract class Location {

    private String preCommand;

    public String getPreCommand() {
        return preCommand;
    }

    public Location setPreCommand(String preCommand) {
        this.preCommand = preCommand;
        return this;
    }
}
