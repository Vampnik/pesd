package eu.ojandu.pesd.manager.docker;


public enum PartitioningMethod {

    REC("-ptype=rb"),
    TMIN("-objtype=vol");

    private final String arguments;

    PartitioningMethod(String arguments) {
        this.arguments = arguments;
    }

    public String getArguments() {
        return arguments;
    }
}
