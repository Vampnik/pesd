package eu.ojandu.pesd.manager.docker;

import eu.ojandu.pesd.common.DockerContainer;
import eu.ojandu.pesd.manager.cmd.CmdUtils;
import eu.ojandu.pesd.manager.cmd.CommandInvokationException;
import eu.ojandu.pesd.manager.cmd.Location;
import org.javatuples.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class TcpDumpTrafficAndUptimeMeter implements TrafficMeter {

    private static Logger log = LoggerFactory.getLogger(TcpDumpTrafficAndUptimeMeter.class);

    private static final String TCPDUMP_TO_FILE = "/net.dump";
    private static final String RES_FILE = "/res.dump";
    private static final Pattern TCPDUMP_PATTERN = Pattern.compile("^[\\w-]+\\.[\\w-]+:?$");


    @Override
    public void setup(Location location, String container) throws CommandInvokationException {
        boolean install = true;
        try {
            install = !CmdUtils.execute(location, "docker exec -u root " + container + "  cat mes.sh").stream()
                    .collect(Collectors.joining(" "))
                    .contains("tcpdump");
        } catch (Exception e) {
            log.info("Checking tcp dump error " + e.getMessage());
        }

        if(install) {
            CmdUtils.execute(location, "docker exec -u root " + container + " apt-get update");
            CmdUtils.execute(location, "docker exec -u root " + container + " apt-get install -y tcpdump");
            CmdUtils.execute(location, "docker exec -u root " + container + " apt-get install -y curl");
            CmdUtils.execute(location, "docker exec -u root " + container + " apt-get install -y procps");
            CmdUtils.execute(location, "docker exec -u root " + container + " curl -L https://bitbucket.org/Vampnik/pesd/raw/master/measure.sh -o mes.sh");
        } else {
            log.info("TCP dump already set up for container: " + container);
        }
    }

    @Override
    public MeasurementResult measure(Location location, String container) throws CommandInvokationException {
        try {
            CmdUtils.execute(location, "docker exec -u root " + container + " bash mes.sh");
        } catch (Exception e) {
        }
        MeasurementResult result = new MeasurementResult();

        List<Pair<Double,Double>> rawResources = CmdUtils.execute(location, "docker exec -u root " + container + " cat " + RES_FILE).stream()
                .map(s -> {
                    String[] split = Arrays.stream(s.split("\\s"))
                            .filter(s1 -> !s1.isEmpty()).toArray(String[]::new);
                    return new Pair<>(Double.parseDouble(split[0]), Double.parseDouble(split[1]));
                }).collect(Collectors.toList());

        double cpuAvg = 0.0;
        double memAvg = 0.0;
        for(Pair<Double, Double> pair : rawResources) {
            cpuAvg += pair.getValue0();
            memAvg += pair.getValue1();
        }

        if(rawResources.size() > 0) {
            cpuAvg /= rawResources.size();
            memAvg /= rawResources.size();
        }

        result.cpu.put(container, cpuAvg);
        result.memory.put(container, memAvg);

        Map<TcpDumpResult, TcpDumpResult> results = new HashMap<>();
        CmdUtils.execute(location, "docker exec -u root " + container + " cat " + TCPDUMP_TO_FILE).stream()
                .forEach(line -> {

                    String source = null;
                    String dest = null;
                    String[] split = line.split(" ");
                    for (String s : split) {
                        if (TCPDUMP_PATTERN.matcher(s).find()) {
                            String[] comps = s.split("\\.");
                            String h = comps[comps.length-1].replace(":", "");
                            if (source == null) {
                                source = h;
                            } else if (dest == null) {
                                dest = h;
                            }
                        }
                    }
                    Long data = null;
                    try {
                        if (split.length > 0) {
                            data = Long.parseLong(split[split.length - 1]);
                        }
                    } catch (NumberFormatException nfe) {
                    }


                    DockerContainer sourceContainer = DockerContainer.getByPort(source);
                    DockerContainer destContainer = DockerContainer.getByPort(dest);

                    if(data != null) {
                        if(sourceContainer != null && !sourceContainer.getName().equals(container)) {
                            increase(results, sourceContainer.getName(), container, data);
                        }
                        if(destContainer != null && !destContainer.getName().equals(container)) {
                            increase(results, destContainer.getName(), container, data);
                        }
                    }
                    if(sourceContainer != null && destContainer != null) {
                        log.warn("Unexpectedly connection with two known ports: " + line);
                    }
                    if(sourceContainer == null && destContainer == null) {
                        log.warn("Unknown traffic: " + line);
                    }

                });
        result.network.addAll(results.values());
        return result;
    }

    private void increase(Map<TcpDumpResult, TcpDumpResult> results, String source, String dest, Long data) {
        TcpDumpResult result = results.get(new TcpDumpResult(source, dest));
        if (result == null) {
            result = new TcpDumpResult(source, dest);
            results.put(result, result);
        }
        result.incrementData(data);
    }
}
