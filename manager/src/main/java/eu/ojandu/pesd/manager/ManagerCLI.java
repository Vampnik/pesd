package eu.ojandu.pesd.manager;

import eu.ojandu.pesd.common.DockerContainer;
import eu.ojandu.pesd.manager.cmd.*;
import eu.ojandu.pesd.manager.docker.*;
import org.apache.log4j.BasicConfigurator;
import org.javatuples.Pair;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ManagerCLI {

    private static final Set<DockerContainer>[] SINGLE_NODE = new Set[1];
    static {
        SINGLE_NODE[0] = Arrays.stream(DockerContainer.values()).collect(Collectors.toSet());
    }

    private static BufferedReader cin;

    public static void main(String[] args) throws IOException, CommandInvokationException {
        BasicConfigurator.configure();
        DockerManager dockerManager = new DockerManager();

        cin = new BufferedReader(new InputStreamReader(System.in));

        while (!Thread.interrupted()) {
            try {
                System.out.print("> ");
                String line = cin.readLine();
                String[] split = line.split("\\s+");
                switch (split[0]) {
                    case "start-local": dockerManager.startContainers(false, DockerManager.LOCAL, new String[] {DockerManager.getHost(DockerManager.LOCAL)}, SINGLE_NODE); break;
                    case "start-swarm": {
                        Set<DockerContainer>[] containers = split.length > 1 ? readGraphFile(split[1]) : SINGLE_NODE;
                        dockerManager.startContainers(true, DockerManager.REMOTE, dmIps(DockerManager.REMOTE, containers.length), containers);
                        break;
                    }
                    case "stop-local": dockerManager.stopContainers(DockerManager.LOCAL, SINGLE_NODE); break;
                    case "stop-swarm": dockerManager.stopContainers(DockerManager.REMOTE, SINGLE_NODE); break;
                    case "measure-local": {
                        MeasurementResult result = dockerManager.measureContainers(new TcpDumpTrafficAndUptimeMeter(), DockerManager.LOCAL, SINGLE_NODE);
                        printMeasurments(result);
                        break;
                    }
                    case "measure-swarm": {
                        MeasurementResult result = dockerManager.measureContainers(new TcpDumpTrafficAndUptimeMeter(), DockerManager.REMOTE, SINGLE_NODE);
                        printMeasurments(result);
                        break;
                    }
                    case "part-local": {
                        Pair<MeasurementResult, Map<PartitioningMethod, Set<DockerContainer>[]>> result = dockerManager.partition(
                                new TcpDumpTrafficAndUptimeMeter(),
                                Integer.parseInt(split[1]),
                                DockerManager.LOCAL,
                                split.length > 2 ? split[2] : null,
                                SINGLE_NODE
                        );
                        printMeasurments(result.getValue0());
                        for(Map.Entry<PartitioningMethod, Set<DockerContainer>[]> entry : result.getValue1().entrySet()) {
                            System.out.println(entry.getKey() + ": " + Arrays.toString(entry.getValue()));
                        }
                        break;
                    }
                    case "part-swarm": {
                        Pair<MeasurementResult, Map<PartitioningMethod, Set<DockerContainer>[]>> result = dockerManager.partition(
                                new TcpDumpTrafficAndUptimeMeter(),
                                Integer.parseInt(split[1]),
                                DockerManager.REMOTE,
                                split.length > 2 ? split[2] : null,
                                SINGLE_NODE
                        );
                        printMeasurments(result.getValue0());
                        for(Map.Entry<PartitioningMethod, Set<DockerContainer>[]> entry : result.getValue1().entrySet()) {
                            System.out.println(entry.getKey() + ": " + Arrays.toString(entry.getValue()));
                        }
                        break;
                    }
                    default: System.out.println("Unknown command: " + line);
                }
                System.out.println("Done");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static void printMeasurments(MeasurementResult result) {
        long total = result.network.stream().mapToLong(r -> r.getData()).sum();
        System.out.println("Total: " + total + " | " + result);
    }

    private static String[] dmIps(Location location, int count) {
        return IntStream.range(2, count+2).mapToObj(i -> {
            try {
                return CmdUtils.execute(location, "docker-machine ip weave-" + i).get(0);
            } catch (CommandInvokationException e) {
                throw new RuntimeException("Could not obtain ip", e);
            }
        }).toArray(String[]::new);
    }

    private static Set<DockerContainer>[] readGraphFile(String file) throws IOException {
        if(file.toLowerCase().startsWith("random")) {
            final int numRandom = Integer.parseInt(file.toLowerCase().substring(6));

            while(true) {
                Set<DockerContainer>[] result = IntStream.range(0, numRandom)
                        .mapToObj(i -> new HashSet<>())
                        .toArray(Set[]::new);

                List<DockerContainer> list = Arrays.asList(DockerContainer.values());
                Collections.shuffle(list);
                DockerContainer[] containers = list.stream().toArray(DockerContainer[]::new);

                for(int i=0;i<containers.length;i++) {
                    result[i % numRandom].add(containers[i]);
                }
                int agent = 2;
                for(Set<DockerContainer> res : result) {
                    System.out.println("agent-" + agent++ + ": " + res);
                }
                System.out.print("Is this layout acceptable (y/N) > ");
                if(cin.readLine().equalsIgnoreCase("y")) {
                    return result;
                }
            }
        } else {
            try (BufferedReader fin = new BufferedReader(new FileReader(file + ".graph"))) {
                return fin.lines().map(l -> Arrays.stream(l.split(" "))
                        .filter(s -> !s.isEmpty())
                        .map(DockerContainer::getByName)
                        .collect(Collectors.toSet())
                ).filter(s -> s.size() > 0).toArray(Set[]::new);
            }
        }
    }
}
