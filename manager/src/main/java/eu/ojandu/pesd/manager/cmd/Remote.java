package eu.ojandu.pesd.manager.cmd;

import org.apache.commons.io.IOUtils;

import java.io.*;

public class Remote extends Location {

    private String host;
    private String user;
    private String password;
    private String identity;

    public Remote(String user, String host) {
        this.user = user;
        this.host = host;
    }

    public Remote setPassword(String password) {
        this.password = password;
        this.identity = null;
        return this;
    }

    public Remote setIndentityFile(String file) throws IOException {
        identity = file;
        password = null;
        //setIndentityFile(new File(file));
        return this;
    }

    public Remote setIndentityFile(File file) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        IOUtils.copy(new FileInputStream(file), baos);
        try {
            identity = new String(baos.toByteArray(), "UTF-8");
            password = null;
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException("UTF-8 is not known");
        }
        return this;
    }

    public String getHost() {
        return host;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public String getIdentity() {
        return identity;
    }

    @Override
    public String toString() {
        return user + "@" + host;
    }
}
