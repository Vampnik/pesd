package eu.ojandu.pesd.manager.cmd;

public class Local extends Location {
    private String dir;

    public Local() {
        this.dir = ".";
    }

    public Local(String dir) {
        this.dir = dir;
    }

    public String getDir() {
        return dir;
    }
}
