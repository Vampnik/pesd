package eu.ojandu.pesd.manager.cmd;

public class CommandInvokationException extends Exception {
    public CommandInvokationException(String message) {
        super(message);
    }

    public CommandInvokationException(String message, Throwable cause) {
        super(message, cause);
    }
}
