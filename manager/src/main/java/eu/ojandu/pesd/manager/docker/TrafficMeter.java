package eu.ojandu.pesd.manager.docker;

import eu.ojandu.pesd.manager.cmd.CommandInvokationException;
import eu.ojandu.pesd.manager.cmd.Location;

public interface TrafficMeter {
    void setup(Location location, String container) throws CommandInvokationException;
    MeasurementResult measure(Location location, String container) throws CommandInvokationException;
}
