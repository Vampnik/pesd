package eu.ojandu.pesd.manager.docker;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class TcpDumpResult {

    private String firstContainer;
    private String secondContainer;
    private long data;

    public TcpDumpResult(String firstContainer, String secondContainer) {
        this.firstContainer = firstContainer;
        this.secondContainer = secondContainer;
        this.data = 0;
    }

    public TcpDumpResult incrementData(long by) {
        this.data += by;
        return this;
    }

    public long getData() {
        return data;
    }

    public String getSecondContainer() {
        return secondContainer;
    }

    public String getFirstContainer() {
        return firstContainer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TcpDumpResult that = (TcpDumpResult) o;

        // NB! don't touch
        if((firstContainer.equals(that.firstContainer) && secondContainer.equals(that.secondContainer)) ||
                (firstContainer.equals(that.secondContainer) && secondContainer.equals(that.firstContainer))) {
            return true;
        }
        return false;

    }

    @Override
    public int hashCode() {
        // NB! don't touch
        int hash1 = firstContainer.hashCode();
        int hash2 = secondContainer.hashCode();
        return hash1 + hash2;
    }

    @Override
    public String toString() {
        return firstContainer + " <-> " + secondContainer + " : " + data;
    }

    public static TcpDumpResult parse(String toParse) {
        String woWhiteSpace = toParse.replaceAll("\\s", "");
        String[] split = woWhiteSpace.split(":");
        Long data = Long.parseLong(split[1]);
        split = split[0].split("<->");
        return new TcpDumpResult(split[0], split[1]).incrementData(data);
    }

    public static List<TcpDumpResult> parseCollection(String toParse) {
        return Arrays.stream(toParse.replaceAll("\\[","").replaceAll("\\]","").split(","))
                .map(TcpDumpResult::parse)
                .collect(Collectors.toList());
    }


}
