package eu.ojandu.pesd.manager.docker;

import eu.ojandu.pesd.common.DockerContainer;
import eu.ojandu.pesd.manager.cmd.*;
import org.javatuples.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class DockerManager {

    private static Logger log = LoggerFactory.getLogger(DockerManager.class);

    private static final String METIS_FILE = "/tmp/tmpMetis";

    public static Location LOCAL;
    public static Location REMOTE;

    static {
        try(InputStream fis = new FileInputStream("access.properties")) {
            Properties properties = new Properties();
            properties.load(fis);

            LOCAL = new Remote(properties.getProperty("local.user"), "localhost").setPassword(properties.getProperty("local.pwd"));
            REMOTE = new Remote(properties.getProperty("remote.user"), properties.getProperty("remote.host"))
                    .setPassword(properties.getProperty("remote.pwd"))
                    .setPreCommand("eval $(docker-machine env --swarm weave-1)");
        } catch (Throwable t) {
            throw new RuntimeException("Could not read access.properties", t);
        }
    }

    public static String getHost(Location location) throws CommandInvokationException {
        Optional<String> eth0 = CmdUtils.execute(location, "/sbin/ifconfig eth0 | grep 'inet addr' | cut -d ':' -f 2 | cut -d ' ' -f 1").stream().findFirst();
        if(eth0.isPresent()) {
            return eth0.get();
        } else {
            return CmdUtils.execute(location, "/sbin/ifconfig wlan0 | grep 'inet addr' | cut -d ':' -f 2 | cut -d ' ' -f 1").stream().findFirst().get();
        }
    }

    private Map<String, DockerContainer> running = new HashMap<>();

    public void startContainers(boolean pull, Location location, String[] ips, final Set<DockerContainer>[] dockerContainers) throws CommandInvokationException {
        log.info("IPS: " + Arrays.toString(ips));
        Map<DockerContainer, String> ipMap = IntStream.range(0, ips.length)
                .mapToObj(i -> dockerContainers[i].stream().map(d -> new Pair<>(d, ips[i])))
                .flatMap(s -> s)
                .collect(Collectors.toMap(p -> p.getValue0(), p -> p.getValue1()));
        int index = 1;
        for(Set<DockerContainer> set : dockerContainers) {
            for(DockerContainer cont : set) {
                log.info("Starting container: {}", cont);
                if(pull) {
                    CmdUtils.execute(location, "docker pull " + cont.getImage());
                }
                CmdUtils.execute(location, cont.buildRun(index+1, ipMap));
                running.put(cont.getName(), cont);
                log.info("Container {} started, sleeping for 10 seconds", cont);
            }
            index++;
        }
    }

    public void stopContainers(Location location, Set<DockerContainer>... dockerContainers)  {
        for(Set<DockerContainer> set : dockerContainers) {
            for (DockerContainer cont : set) {
                try {
                    log.info("Starting stopped: {}", cont);
                    CmdUtils.execute(location, "docker stop " + cont.getName());
                    CmdUtils.execute(location, "docker rm " + cont.getName());
                    running.remove(cont.getName());
                    log.info("Container {} stopped.", cont);
                } catch (CommandInvokationException e) {
                    log.error("Encountered exception: ", e);
                }

            }
        }
    }

    public static MeasurementResult measureContainers(TrafficMeter meter, Location location, Set<DockerContainer>... dockerContainers) throws CommandInvokationException {
        List<Thread> threads = new LinkedList<>();

        for(Set<DockerContainer> containerSet : dockerContainers) {
            for (DockerContainer container : containerSet) {
                Thread thread = new Thread(() -> {
                    try {
                        meter.setup(location, container.getName());
                    } catch (CommandInvokationException e) {
                        log.error("Error when monitoring container: " + container.getName(), e);
                    }
                });
                thread.start();
                threads.add(thread);
            }
        }
        for(Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                throw new CommandInvokationException("Unexpected interruption", e);
            }
        }

        MeasurementResult measurementResult = new MeasurementResult();
        Map<TcpDumpResult, TcpDumpResult> map = new HashMap<>();
        for(Set<DockerContainer> containerSet : dockerContainers) {
            for (DockerContainer container : containerSet) {
                Thread thread = new Thread(() -> {
                    try {
                        MeasurementResult rawRes = meter.measure(location, container.getName());
                        rawRes.cpu.entrySet().forEach(e -> measurementResult.cpu.put(e.getKey(), e.getValue()));
                        rawRes.memory.entrySet().forEach(e -> measurementResult.memory.put(e.getKey(), e.getValue()));
                        for(TcpDumpResult res : rawRes.network) {
                            TcpDumpResult inMap = map.get(res);
                            if(inMap == null || res.getData() > inMap.getData()) {
                                map.put(res, res);
                            }
                        }
                    } catch (CommandInvokationException e) {
                        log.error("Error when monitoring container: " + container.getName(), e);
                    }
                });
                thread.start();
                threads.add(thread);
            }
        }

        for(Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                throw new CommandInvokationException("Unexpected interruption", e);
            }
        }

        double cpuSum = measurementResult.cpu.values().stream().mapToDouble(d -> d).sum();
        double memSum = measurementResult.memory.values().stream().mapToDouble(d -> d).sum();
        for(DockerContainer container : DockerContainer.values()) {
            Double cpu = measurementResult.cpu.get(container.getName());
            Double mem = measurementResult.memory.get(container.getName());
            if(cpu != null) {
                measurementResult.cpu.put(container.getName(), cpu / cpuSum * 100);
            }
            if(mem != null) {
                measurementResult.memory.put(container.getName(), mem / memSum * 100);
            }
        }

        measurementResult.network.addAll(map.values());
        return measurementResult;
    }

    public static Pair<MeasurementResult, Map<PartitioningMethod, Set<DockerContainer>[]>> partition(TrafficMeter meter, int partition, Location location, String outputFile, Set<DockerContainer>... dockerContainers) throws CommandInvokationException, IOException {
        MeasurementResult measureResult = measureContainers(meter, location, dockerContainers);

        int last = 0;
        Map<String, Integer> indices = new HashMap<>();

        DockerContainer[] containers = DockerContainer.values();
        long[][] weights = new long[containers.length][containers.length];
        for(int i = 0; i< containers.length; i++) {
            for(int j = 0; j< DockerContainer.values().length; j++) {
                weights[i][j] = -1;
            }
        }

        long max = -1;

        for(TcpDumpResult result : measureResult.network) {
            Integer index1 = indices.get(result.getFirstContainer());
            if(index1 == null) {
                index1 = last++;
                indices.put(result.getFirstContainer(), index1);
            }

            Integer index2 = indices.get(result.getSecondContainer());
            if(index2 == null) {
                index2 = last++;
                indices.put(result.getSecondContainer(), index2);
            }

            weights[index1][index2] = result.getData();
            weights[index2][index1] = result.getData();

            max = Math.max(result.getData(), max);
        }

        if(max > 100000) {
            for(int i=0;i<weights.length;i++) for(int j=0;j<weights[i].length;j++) {
                if(weights[i][j] > 0) {
                    weights[i][j] = (long)(100 * Math.log(weights[i][j]));
                    if(weights[i][j] < 1) {
                        weights[i][j] = 1;
                    }
                }
            }
        }

        Map<Integer, String> indices2 = indices.entrySet().stream().collect(Collectors.toMap(e -> e.getValue(), e -> e.getKey()));

        PrintWriter fout = null;
        try {
            fout = new PrintWriter(new FileWriter(METIS_FILE));
            fout.println(last + " " + measureResult.network.size() + " 011");
            DockerContainer.values();
            for(int i=0;i<containers.length;i++) {
                String contString = indices2.get(i);
                fout.print((measureResult.cpu.get(contString).intValue()) + " ");
                for(int j=0;j<containers.length;j++) {
                    if(weights[i][j] > -1) {
                        fout.print(j+1 + " " + weights[i][j] + " ");
                    }
                }
                fout.println();
            }
        } catch (IOException e) {
            throw new CommandInvokationException("Could now write partition", e);
        } finally {
            if(fout != null) {
                fout.close();
            }
        }


        Map<PartitioningMethod, Set<DockerContainer>[]> results = new HashMap<>();
        for(PartitioningMethod method : PartitioningMethod.values()) {
            CmdUtils.execute(LOCAL, "gpmetis " + METIS_FILE + " " + partition + " " + method.getArguments());
            System.out.println("|" + CmdUtils.execute(LOCAL, "cat " + METIS_FILE + ".part." + partition) + "|");
            List<String> partitionStrings = CmdUtils.execute(LOCAL, "cat " + METIS_FILE + ".part." + partition);

            Set<DockerContainer>[] partResult = new Set[partition];
            for(int i=0;i<partition;i++) {
                partResult[i] = new HashSet<>();
            }

            int index = 0;
            for(String part : partitionStrings) {
                partResult[Integer.parseInt(part)].add(DockerContainer.getByName(indices2.get(index++)));
            }

            if(outputFile != null) {
                try(PrintWriter fileOut = new PrintWriter(new FileWriter(outputFile + "-" + method + ".graph"))) {
                    for(Set<DockerContainer> set : partResult) {
                        for(DockerContainer cont : set) {
                            fileOut.print(cont.getName() + " ");
                        }
                        fileOut.println();
                    }
                }
            }
            results.put(method, partResult);
        }




        return new Pair<>(measureResult, results);
    }
}
