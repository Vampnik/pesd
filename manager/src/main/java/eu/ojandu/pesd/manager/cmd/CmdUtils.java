package eu.ojandu.pesd.manager.cmd;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.PumpStreamHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class CmdUtils {

    private static Logger log = LoggerFactory.getLogger(CmdUtils.class);

    public static List<String> execute(Location location, String cmd) throws CommandInvokationException {
        String resCmd = (location.getPreCommand() != null ? location.getPreCommand() + " && " : "") + cmd;
        log.debug("Executing command: " + resCmd);
        if(location instanceof Local) {
            return executeLocal((Local) location, resCmd);
        } else if(location instanceof Remote) {
            return executeRemote((Remote) location, resCmd);
        } else {
            throw new IllegalArgumentException("Unknown location type. Fix code: " + location.getClass());
        }
    }

    public static List<String> executeLocal(String cmd) throws CommandInvokationException {
        return executeLocal(new Local("."), cmd);
    }

    private static List<String> executeLocal(Local local, String cmd) throws CommandInvokationException {
        CommandLine oCmdLine = CommandLine.parse(cmd);
        DefaultExecutor executor = new DefaultExecutor();
        executor.setExitValue(0);
        executor.setWorkingDirectory(new File(local.getDir()));

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        executor.setStreamHandler(new PumpStreamHandler(baos));
        try {
            Map<String, String> env = new HashMap<>();
            env.put("ASD", "DSA");
            int exitValue = executor.execute(oCmdLine, env);
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            List<String> lines = getLines(bais);
            validateExitStatus(exitValue, lines);
            return lines;
        } catch (IOException e) {
            String lines = "";
            try {
                ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
                lines = getLines(bais).stream().collect(Collectors.joining("\n"));
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            throw new CommandInvokationException("Could not execute command " + cmd + ":\n" + lines, e);
        }
    }

    private static List<String> executeRemote(Remote remote, String cmd) throws CommandInvokationException {
        try {
            JSch jsch = new JSch();

            if(remote.getIdentity() != null) {
                jsch.addIdentity(remote.getIdentity());
            }

            Session session = jsch.getSession(remote.getUser(), remote.getHost());
            session.setConfig("StrictHostKeyChecking", "no");
            if(remote.getPassword() != null) {
                session.setPassword(remote.getPassword());
            }
            session.connect();

            ChannelExec channelExec = (ChannelExec)session.openChannel("exec");

            InputStream in = channelExec.getInputStream();

            System.out.println("Running command on remote " + remote + " \n\t" + cmd);

            channelExec.setCommand(cmd);
            channelExec.connect();

            List<String> result = getLines(in);

            int exitStatus = channelExec.getExitStatus();
            channelExec.disconnect();
            session.disconnect();
            validateExitStatus(exitStatus, result);

            return result;
        } catch (IOException | JSchException e) {
            throw new CommandInvokationException("Could not execute command " + cmd, e);
        }
    }

    private static List<String> getLines(InputStream in) throws IOException {
        List<String> result = new LinkedList<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));

        String line;
        while ((line = reader.readLine()) != null) {
            result.add(line);
        }
        return result;
    }

    private static void validateExitStatus(int exitStatus, List<String> result) throws CommandInvokationException {
        if(exitStatus < 0) {
            log.warn("Done, but exit status not set!");
        } else if(exitStatus > 0){
            throw new CommandInvokationException("Done, but with error: " + exitStatus + " " + result);
        }
    }
}
