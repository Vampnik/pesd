package eu.ojandu.pesd.manager.docker;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MeasurementResult {
    public Map<String, Double> cpu = new HashMap<>();
    public Map<String, Double> memory = new HashMap<>();
    public Set<TcpDumpResult> network = new HashSet<>();

    public MeasurementResult add(MeasurementResult other) {
        other.cpu.entrySet().stream().forEach(e -> cpu.put(e.getKey(), e.getValue()));
        other.memory.entrySet().stream().forEach(e -> memory.put(e.getKey(), e.getValue()));
        network.addAll(other.network);
        return this;
    }

    @Override
    public String toString() {
        return "MeasurementResult{" +
                "cpu=" + cpu +
                ", memory=" + memory +
                ", network=" + network +
                '}';
    }
}
