package eu.ojandu.pesd.manager;

import org.javatuples.Pair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class LoadTest {

    private static final long OPTIMAL_REQ_TIME = 1000L;

    private static class RecursiveTask implements Runnable {

        private AvgHolder avgHolder;

        public RecursiveTask(AvgHolder avgHolder, String url, int number, AtomicInteger taskCount) {
            this.avgHolder = avgHolder;
            this.url = url;
            this.number = number;
            this.taskCount = taskCount;
        }

        private String url;

        private int number;

        private AtomicInteger taskCount;

        @Override
        public void run() {
            boolean cont = true;
            while(cont) {
                int before = taskCount.get();
                long start = System.currentTimeMillis();
                boolean except = false;
                try {

                    get(url);
                } catch (Exception e) {
                    except = true;
                    //e.printStackTrace();
                    //System.out.println(except);
                }
                long end = System.currentTimeMillis();
                avgHolder.aggregate(new Pair<>(end, (end-start)));
                Pair<Long, Long> avg = avgHolder.get();
                int after = taskCount.get();
                if(before == after && after == number) {
                    if(!except && avg.getValue1() < OPTIMAL_REQ_TIME) {
//                        int toAdd = (int) ((OPTIMAL_REQ_TIME - (end-start)) / 2.0);
//                        if(toAdd < 1) toAdd = 1;
//
//                        List<Thread> list = IntStream.range(0, toAdd)
//                                .mapToObj(i -> new Thread(new RecursiveTask(avgHolder, url, taskCount.incrementAndGet(), taskCount)))
//                                .collect(Collectors.toList());
//
//                        for(Thread task : list) task.start();

                        new Thread(new RecursiveTask(avgHolder, url, taskCount.incrementAndGet(), taskCount)).start();
                    } else if((except || avg.getValue1() > OPTIMAL_REQ_TIME) && number > 1) {
                        taskCount.decrementAndGet();
                        cont = false;
                    }
                }
            }

        }
    }

    private static class AvgHolder {

        private static final long PERIOD = 60;

        private List<Pair<Long, Long>> times = new LinkedList<>();

        private void aggregate(Pair<Long, Long> toAdd) {
            synchronized (this) {
                times.add(toAdd);
            }
        }

        private Pair<Long, Long> get() {
            synchronized (this) {
                long limit = System.currentTimeMillis()-(PERIOD*1000);
                long total = 0;
                long avg = 0;
                Iterator<Pair<Long, Long>> iter = times.iterator();
                while(iter.hasNext()) {
                    Pair<Long, Long> next = iter.next();
                    if(next.getValue0() < limit) {
                        iter.remove();
                    } else {
                        total++;
                        if(avg < 0) {
                            avg = avg == 0 ? 100 : avg*100/10;
                        } else {
                            //System.out.println(next.getValue1());
                            avg += next.getValue1();
                        }
                    }
                }
                return new Pair<>(total, total == 0 ? 0 : avg / total);
            }
        }
    }

    private static class RequestRunner {

        private final Thread thread;
        private String name;
        private String url;
        private long reqPerThread;

        private AvgHolder avgHolder = new AvgHolder();

        private AtomicInteger count = new AtomicInteger(1);

        public RequestRunner(String name, String url) {

            this.thread = new Thread(new RecursiveTask(avgHolder, url, 1, count));
            this.name = name;
            this.url = url;
        }

        private RequestRunner start() {
            thread.start();
            return this;
        }

        private String report() {
            return name + "(" + count.get() + ")-" + avgHolder.get();
        }
    }


    public static void main(String[] args) throws InterruptedException {
        RequestRunner evgen = new RequestRunner("EVGEN",  "http://jmtest/evgen").start();
        RequestRunner stat = new RequestRunner("STAT",  "http://jmtest/api/stat").start();
        RequestRunner chars = new RequestRunner("CHARS",  "http://jmtest/api/chars").start();
        RequestRunner logs = new RequestRunner("LOGS",  "http://jmtest/api/logs").start();

        while(true) {
            Thread.sleep(1000);
            System.out.println("--------------");
            System.out.println(evgen.report());
            System.out.println(stat.report());
            System.out.println(chars.report());
            System.out.println(logs.report());
        }
    }

    public static String get(String targetUrl) throws IOException {
        HttpURLConnection connection = null;
        try {
            //Create connection
            URL url = new URL(targetUrl);
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");

            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoOutput(true);

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder(); // or StringBuffer if not Java 5+
            String line;
            while((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            return response.toString();
        } catch (Exception e) {
            //e.printStackTrace();
            throw e;
        } finally {
            if(connection != null) {
                connection.disconnect();
            }
        }
    }
}
