#!/bin/bash

#timeout 10 tcpdump -i ethwe | cut -d ' ' -f 3,5,14 > /net.dump
#timeout 10 tcpdump -i ethwe > /net.dump
#timeout 60 tcpdump -i eth0 > /net.dump

total=60
dir=/

rm -f ${dir}net.dump
rm -f ${dir}res.dump
timeout ${total} tcpdump -i eth0 > ${dir}net.dump & for i in $(seq 1 ${total}) ; do ps -e -o %cpu,%mem,cmd | tail -n +2 | awk '{ cpu+=$1; mem+=$2 } END { print cpu"\t"mem }' | tail -1 >> ${dir}res.dump  ; sleep 1; done;