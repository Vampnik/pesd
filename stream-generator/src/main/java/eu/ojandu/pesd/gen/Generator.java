package eu.ojandu.pesd.gen;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.sun.net.httpserver.HttpServer;
import eu.ojandu.pesd.common.*;
import eu.ojandu.pesd.common.AMQP;
import eu.ojandu.pesd.common.AbstractLifecycle;
import eu.ojandu.pesd.common.MessageSender;
import eu.ojandu.pesd.common.Utils;
import eu.ojandu.pesd.common.dto.Event;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.Random;

@Configuration
@ComponentScan("eu.ojandu.pesd")
@EnableAutoConfiguration
public class Generator {

    private static Logger log = LoggerFactory.getLogger(Generator.class);

    private static Random rand = new Random();

    private static String host = "localhost";

    private static Integer sleepFor = null;

    private static final int EVENTS_PER_REQ = 1;

    @Bean
    public AutowiredAnnotationBeanPostProcessor aw() {
        return new AutowiredAnnotationBeanPostProcessor();
    }

    @Bean
    public AMQP amqp() {
        return new AMQP(host);
    }

    @Bean
    public MessageSender<Event> messageSender() {
        return amqp().buildSender(Utils.EVENT_QUEUE, Event.class);
    }

    @Bean
    public AbstractLifecycle eventGenerator() throws IOException {
        return new AbstractLifecycle() {
            private Thread thread;
            private HttpServer server = HttpServer.create(new InetSocketAddress(8080), 0);

            @Override
            protected void doStart() {
                MessageSender<Event> sender = messageSender();
                if(sleepFor != null) {
                    thread = new Thread(() -> {
                        while (!Thread.interrupted()) {
                            try {
                                send(sender);
                                Thread.sleep(sleepFor);
                            } catch (Exception e) {
                                log.error("Unexpected exception", e);
                            }
                        }
                    });
                    thread.start();
                }
            }

            @Override
            protected void doStop() {
                if(thread != null) {
                    thread.interrupt();
                }
                server.stop(0);
            }
        };
    }

    public static void send(MessageSender<Event> sender) {
        for(int i=0;i<EVENTS_PER_REQ;i++) {
            doSend(sender);
        }
    }

    private volatile static int msgCount = 0;
    private volatile static long msgCountChecked = -1;
    private volatile static boolean checking = false;
    private static Object msgLock = new Object();

    private static int getMsgCount() throws UnirestException {
        if(msgCountChecked < System.currentTimeMillis() + 10000 && !checking) {
            synchronized (msgLock) {
                checking = true;
                HttpResponse<JsonNode> json = Unirest.get("http://guest:guest@" + host + ":15672/api/vhosts").asJson();
                msgCount = json.getBody()
                        .getArray()
                        .getJSONObject(0)
                        .getInt("messages_ready");
                msgCountChecked = System.currentTimeMillis();
                checking = false;
            }
        }
        return msgCount;
    }

    private static void doSend(MessageSender<Event> sender) {
        long start = System.currentTimeMillis();
        try {
            int messages = getMsgCount();
//            int toSleep = (int) Math.pow(Math.E, messages / 10.0)*1000;
            int toSleep = messages * 100;
            if(toSleep > 120000)  {
                throw new IllegalStateException("Have to wait too long: " + toSleep);
            }
            log.info("ToSleep: " + toSleep);
            Thread.sleep(toSleep);
        } catch (Exception e) {
            log.error("Could not obtain message queue size: ", e);
        }
        sender.send(generateEvent());
        log.info("Total time spent on sending: " + (System.currentTimeMillis() - start));
    }

    public static void main(String[] args) {
        log.info("Starting EvGen with args: " + Arrays.toString(args));
        if(args.length > 0) {
            host = args[0];
        }
        log.info("Starting mq toward: " + host);
        if(args.length > 1) {
            sleepFor = Integer.parseInt(args[1]);
        }

        SpringApplication.run(Generator.class, args);
    }

    //private static char[] bigPayload = RandomStringUtils.randomAlphabetic(1024*1024).toCharArray();
    private static char[] bigPayload = RandomStringUtils.randomAlphabetic(5).toCharArray();
    private static Event generateEvent() {
        bigPayload[rand.nextInt(bigPayload.length)] = RandomStringUtils.random(1).charAt(0);
        Event event = new Event(RandomStringUtils.randomAlphabetic(10), Utils.RANDOM.nextInt(), new String(bigPayload));
        log.info("Event generated: " + event);
        return event;
    }
}
