package eu.ojandu.pesd.gen;

import eu.ojandu.pesd.common.MessageSender;
import eu.ojandu.pesd.common.dto.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Ctrl {

    @Autowired
    private MessageSender<Event> messageSender;

    @RequestMapping("/evgen")
    public String evgen() {
        Generator.send(messageSender);
        return "";
    }
}
