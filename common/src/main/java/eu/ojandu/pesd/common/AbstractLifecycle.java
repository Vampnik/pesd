package eu.ojandu.pesd.common;

import org.springframework.context.SmartLifecycle;

public abstract class AbstractLifecycle implements SmartLifecycle {

    private boolean running = false;

    @Override
    public boolean isAutoStartup() {
        return true;
    }

    @Override
    public void stop(Runnable runnable) {
        stop();
        runnable.run();
    }

    @Override
    public void start() {
        doStart();
        running = true;
    }

    @Override
    public void stop() {
        doStop();
        running = false;
    }

    @Override
    public boolean isRunning() {
        return running;
    }

    @Override
    public int getPhase() {
        return 0;
    }

    protected abstract void doStart();

    protected abstract void doStop();
}
