package eu.ojandu.pesd.common.repo;

import eu.ojandu.pesd.common.entity.Character;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharacterRepo extends CrudRepository<Character, Long> {

    Character getByValue(String value);

}
