package eu.ojandu.pesd.common;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public enum DockerContainer {

    // Aliases taken from https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xml
    RABBIT_MQ("rabbitmq:3-management", "rmq", "-p 15672:15672 -p 5672:5672 -p 5671:5671", "", new DockerContainer[0], new String[]{"5672", "5671", "amqp", "amqps", "15672"}),
    SQL("postgres", "sql", "-e POSTGRES_USER=ew -e POSTGRES_PASSWORD=ew -e POSTGRES_USER=ew -e POSTGRES_PASSWORD=ew -p 5432:5432", "", new DockerContainer[0], new String[]{"5432", "postgresql"}),
    MONGODB("mongo", "mongoDb", "-p 27017:27017", "", new DockerContainer[0], new String[]{"27017"}),
    ELASTIC_SEARCH("elasticsearch:2.1.1", "elasticsearch", "-p 9200:9200", "", new DockerContainer[0], new String[]{"9200", "wap-wsp"}),
    LOGSTASH("vampnik/pesd-logstash", "logstash", "-p 4560:4560", "logstash agent -f /logstash.conf", new DockerContainer[]{ELASTIC_SEARCH}, new String[]{"4560"}),
    KIBANA("kibana:4.3.1", "kibana", "-e ELASTICSEARCH_URL=http://elasticsearch:9200 -p 5601:5601", "", new DockerContainer[]{ELASTIC_SEARCH}, new String[]{"5601", "esmagent"}),
    EVENT_GENERATOR("vampnik/pesd-stream-generator", "evGen", "-p 8082:8082", "rmq", new DockerContainer[] {RABBIT_MQ, LOGSTASH}, new String[]{"8082", "sunproxyadmin"}),
    MIDDLEWARE("vampnik/pesd-mw", "mw", "", "rmq", new DockerContainer[]{RABBIT_MQ, LOGSTASH, MONGODB, SQL}, new String[0]),
    REST("vampnik/pesd-rest", "rest", "-p 8081:8081", "rmq", new DockerContainer[]{RABBIT_MQ, LOGSTASH, MONGODB, SQL, KIBANA}, new String[] {"8081", "us-cli", "tproxy"}),
    NGINX("vampnik/pesd-fe", "nginx", "-p 80:80", "", new DockerContainer[]{REST, EVENT_GENERATOR}, new String[] {"http", "80"});

    private String image;
    private String name;
    private String additional;
    private String imageArg;
    private DockerContainer[] dependsOn;
    private String[] ports;

    private static Map<String, DockerContainer> portMap = new HashMap<>();

    DockerContainer(String image, String name, String additional, String imageArg, DockerContainer[] dependsOn, String[] ports) {
        this.image = image;
        this.name = name;
        this.additional = additional;
        this.imageArg = imageArg;
        this.dependsOn = dependsOn;
        this.ports = ports;
    }

    public String getImage() {
        return image;
    }

    public String getName() {
        return name;
    }

    public String buildRun(long agentNum, final Map<DockerContainer, String> ips) {
        return "docker run --restart=\"always\" -d -e constraint:agentid==agent" + agentNum + " "
                + Arrays.stream(dependsOn).map(d -> "--add-host " + d.getName() + ":" + ips.get(d)).collect(Collectors.joining(" ")) + " --name "
                + name + " " + additional + " " + image + " " + imageArg;
    }

    public static boolean isValidName(String n) {
        for(DockerContainer dc : DockerContainer.values()) {
            if(dc.getName().equals(n)) {
                return true;
            }
        }
        return false;
    }

    public static DockerContainer getByName(String name) {
        for(DockerContainer dc : DockerContainer.values()) {
            if(dc.getName().equals(name)) {
                return dc;
            }
        }
        return null;
    }

    public static DockerContainer getByPort(String port) {
        return portMap.get(port);
    }

    static {
        for(DockerContainer c : DockerContainer.values()) {
            for(String port : c.ports) {
                portMap.put(port, c);
            }
        }
    }
}
