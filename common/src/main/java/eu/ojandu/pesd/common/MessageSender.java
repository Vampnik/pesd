package eu.ojandu.pesd.common;

import org.springframework.amqp.rabbit.core.RabbitTemplate;

public class MessageSender<T> {
    private final RabbitTemplate template;
    private final String exchange;
    private final String routingKey;

    public MessageSender(RabbitTemplate template, String exchange, String routingKey) {
        this.template = template;
        this.exchange = exchange;
        this.routingKey = routingKey;
    }

    public void send(T message) {
        template.convertAndSend(exchange, routingKey, Utils.GSON.toJson(message));
    }
}
