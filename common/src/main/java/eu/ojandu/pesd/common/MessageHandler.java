package eu.ojandu.pesd.common;

public abstract class MessageHandler<T> {
    private final Class<T> classOfT;

    public MessageHandler(Class<T> classOfT) {
        this.classOfT = classOfT;
    }

    public void handleMessage(String message) {
        handle(Utils.GSON.fromJson(message, classOfT));
    }

    protected abstract void handle(T t);
}
