package eu.ojandu.pesd.common;

import com.mongodb.*;
import com.mongodb.util.JSON;
import eu.ojandu.pesd.common.dto.MongoObj;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.UnknownHostException;
import java.util.*;
import java.util.stream.Collectors;

public class MongoService {

    private static Logger log = LoggerFactory.getLogger(MongoService.class);

    private MongoClient mongoClient = null;
    private DB database = null;

    private String dbHost = "mongoDb";
    private int dbPort = 27017;
    private String dbName = "local";

    public <T extends MongoObj> void insertOrSave(String collection, T obj) {
        WriteResult res = obj._id != null ?
                getCollection(collection).save((DBObject) JSON.parse(Utils.GSON.toJson(obj))) :
                getCollection(collection).insert((DBObject) JSON.parse(Utils.GSON.toJson(obj)));
    }

    public <T extends MongoObj>  Collection<T> find(String collection, String criteria, final Class<T> classOfT) {
        DBCollection col = getCollection(collection);

        List<String> result = new LinkedList<>();
        col.find((DBObject) JSON.parse(criteria)).forEach(o -> result.add(JSON.serialize(o)));

        return result.stream().map(s -> Utils.GSON.fromJson(s, classOfT)).collect(Collectors.toList());
    }

    private MongoClient getClient() {
        if (mongoClient == null) {
            try {
                log.info("connecting to MongoDB at " + dbHost + ":" + dbPort);
                mongoClient = new MongoClient(dbHost, dbPort);
            } catch (UnknownHostException e) {
                log.error("connection failed:", e);
                throw new RuntimeException("Connection failed: "
                        + e.getMessage());
            }
        }
        return mongoClient;
    }

    private DB getDatabase() {
        log.info("using database: " + dbName);
        if(database == null) {
            database = getClient().getDB(dbName);
        }
        return database;
    }

    private DBCollection getCollection(String collection) {
        DBCollection dbCollection = getDatabase().getCollection(collection);
        return dbCollection;
    }
}
