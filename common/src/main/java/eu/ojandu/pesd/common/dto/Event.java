package eu.ojandu.pesd.common.dto;

import java.io.Serializable;

public class Event implements Serializable {
    private String name;
    private int value;
    private String extraPayload;

    public Event(String name, int value, String extraPayload) {
        this.name = name;
        this.value = value;
        this.extraPayload = extraPayload;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public String getExtraPayload() {
        return extraPayload;
    }

    @Override
    public String toString() {
        return "Event{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
