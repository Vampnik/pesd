package eu.ojandu.pesd.common.dto;

import java.io.Serializable;

public class Filter implements Serializable {
    private int lessThan;
    private String filterChars;

    public int getLessThan() {
        return lessThan;
    }

    public void setLessThan(int lessThan) {
        this.lessThan = lessThan;
    }

    public String getFilterChars() {
        return filterChars;
    }

    public void setFilterChars(String filterChars) {
        this.filterChars = filterChars;
    }
}
