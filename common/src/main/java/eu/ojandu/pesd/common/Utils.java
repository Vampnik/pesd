package eu.ojandu.pesd.common;

import com.google.gson.Gson;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Random;

public class Utils {

    public static final Gson GSON = new Gson();
    public static final Random RANDOM = new Random();

    public static final String EVENT_QUEUE = "eventQueue";
    public static final String FILTER_QUEUE = "filterQueue";
    public static final String EXCAHNGE = "fullExchange";

    public static final String MONGO_STAT_COLLECTION = "statistics";

    public static void sleepUntilInterrupt(Runnable finallyBlock) {
        try {
            while(!Thread.interrupted()) {
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {

                }
            }
        } finally {
            finallyBlock.run();
        }

    }

}
