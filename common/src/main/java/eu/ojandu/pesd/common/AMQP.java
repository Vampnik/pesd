package eu.ojandu.pesd.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;

import java.io.Closeable;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class AMQP implements Closeable {

    private static Logger log = LoggerFactory.getLogger(AMQP.class);

    private ConnectionFactory cf;
    private RabbitAdmin admin;
    private TopicExchange exchange;
    private RabbitTemplate template;

    private List<SimpleMessageListenerContainer> containers = new LinkedList<>();

    public AMQP(String host) {
        cf = new CachingConnectionFactory(host);

        admin = new RabbitAdmin(cf);
        exchange = new TopicExchange(Utils.EXCAHNGE);

        admin.declareExchange(exchange);

        template = new RabbitTemplate(cf);
    }

    public <T> void registerHandler(String queueName, MessageHandler<T> handler) {
        Queue queue = new Queue(queueName);
        admin.declareQueue(queue);
        admin.declareBinding(BindingBuilder.bind(queue).to(exchange).with(Utils.EXCAHNGE + "." + queueName));

        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(cf);
        MessageListenerAdapter adapter = new MessageListenerAdapter(handler);
        container.setMessageListener(adapter);
        container.setQueueNames(queueName);
        container.start();
    }

    public <T> MessageSender<T> buildSender(String queueName, Class<T> tClass) {
        return new MessageSender<>(template, Utils.EXCAHNGE, Utils.EXCAHNGE + "." + queueName);
    }

    @Override
    public void close() throws IOException {
        containers.forEach(c -> {
            try {
                c.stop();
            } catch (Throwable t) {
                log.error("Failed to close", t);
            }
        });
    }
}
