package eu.ojandu.pesd.mw;

import eu.ojandu.pesd.common.AbstractLifecycle;
import eu.ojandu.pesd.common.MongoService;
import eu.ojandu.pesd.common.Utils;
import eu.ojandu.pesd.common.dto.Statistics;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;
import java.util.Random;

public class EventAggregator extends AbstractLifecycle {

    private static Random rand = new Random();

    private static Logger log = LoggerFactory.getLogger(EventAggregator.class);

    @Autowired
    private MongoService mongoService;

    private Thread thread;

    private Object lock = new Object();

    private volatile int filterCounter = 0;
    private volatile int totalCounter = 0;

    public void filterEvent() {
        synchronized (lock) {
            filterCounter++;
            totalCounter++;
        }
    }

    public void passEvent() {
        synchronized (lock) {
            totalCounter++;
        }
    }

    //private static char[] bigPayload = RandomStringUtils.randomAlphabetic(1024).toCharArray();
    private static char[] bigPayload = RandomStringUtils.randomAlphabetic(5).toCharArray();
    @Override
    protected void doStart() {
        thread = new Thread(() -> {
            while (!Thread.interrupted()) {
                try {
                    synchronized (lock) {
                        Optional<Statistics> opt = mongoService.find(Utils.MONGO_STAT_COLLECTION, "{}", Statistics.class).stream().findFirst();
                        Statistics stat;
                        if(opt.isPresent()) {
                            stat = opt.get();
                        } else {
                            stat = new Statistics();
                        }
                        bigPayload[rand.nextInt(bigPayload.length)] = RandomStringUtils.random(1).charAt(0);
                        stat.payload = new String(bigPayload);
                        stat.filtered += filterCounter;
                        stat.total += totalCounter;
                        filterCounter = 0;
                        totalCounter = 0;
                        mongoService.insertOrSave(Utils.MONGO_STAT_COLLECTION, stat);
                    }
                    Thread.sleep(5000);
                } catch (Exception e) {
                    log.error("Unexpected exception", e);
                }
            }
        });
        thread.start();
    }

    @Override
    protected void doStop() {
        if(thread != null) {
            thread.interrupt();
        }
    }
}
