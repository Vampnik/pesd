package eu.ojandu.pesd.mw;

import eu.ojandu.pesd.common.AMQP;
import eu.ojandu.pesd.common.MongoService;
import eu.ojandu.pesd.common.Utils;
import liquibase.integration.spring.SpringLiquibase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.persistence.EntityManagerFactory;
import java.util.Properties;

@Configuration
@EnableJpaRepositories(basePackages = "eu.ojandu.pesd.common.repo")
public class MW {

    private static String host = "localhost";

    @Bean
    public AutowiredAnnotationBeanPostProcessor aw() {
        return new AutowiredAnnotationBeanPostProcessor();
    }

    @Bean
    public EventAggregator eventAggregator() {
        return new EventAggregator();
    }

    @Bean
    public MongoService mongoService() {
        return new MongoService();
    }

    @Bean
    public EventHandler eventHandler() {
        return new EventHandler();
    }

    @Bean
    public FilterHandler filterHandler() {
        return new FilterHandler();
    }

    @Bean
    public SpringLiquibase liquibase() {
        SpringLiquibase liquibase = new SpringLiquibase();
        liquibase.setChangeLog("classpath:db/changelog/db.changelog-master.yaml");
        liquibase.setDataSource(dataSource());
        return liquibase;
    }

    @Bean
    public AMQP amqp() {
        AMQP amqp = new AMQP(host);
        amqp.registerHandler(Utils.EVENT_QUEUE, eventHandler());
        amqp.registerHandler(Utils.FILTER_QUEUE, filterHandler());
        return amqp;
    }

    @Bean
    public Properties jpaProps() {
        Properties props = new Properties();
        props.put("hibernate.ejb.naming_strategy", "org.hibernate.cfg.ImprovedNamingStrategy");
        props.put("hibernate.show_sql", false);
        props.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
        return props;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setDataSource(dataSource());
        factory.setPackagesToScan("eu.ojandu.pesd.common.entity");
        HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
        adapter.setDatabase(Database.POSTGRESQL);
        adapter.setGenerateDdl(false);
        factory.setJpaVendorAdapter(adapter);
        factory.setJpaProperties(jpaProps());
        return factory;
    }

    @Bean
    public DriverManagerDataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl("jdbc:postgresql://sql:5432/ew");
        dataSource.setUsername("ew");
        dataSource.setPassword("ew");
        return dataSource;
    }

    @Autowired
    EntityManagerFactory entMan;

    @Bean
    public JpaTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entMan);
        return transactionManager;
    }

    public static void main(String[] args) {
        if(args.length > 0) {
            host = args[0];
        }

        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(MW.class);
        ctx.start();

        Utils.sleepUntilInterrupt(() -> ctx.stop());
    }
}
