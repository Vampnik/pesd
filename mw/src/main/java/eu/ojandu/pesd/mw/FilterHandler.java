package eu.ojandu.pesd.mw;

import eu.ojandu.pesd.common.MessageHandler;
import eu.ojandu.pesd.common.dto.Filter;
import org.springframework.beans.factory.annotation.Autowired;

public class FilterHandler extends MessageHandler<Filter> {

    @Autowired
    private EventHandler eventHandler;

    public FilterHandler() {
        super(Filter.class);
    }

    @Override
    protected void handle(Filter filter) {
        eventHandler.setFilterChars(filter.getFilterChars().toCharArray());
        eventHandler.setLessThan(filter.getLessThan());
    }
}
