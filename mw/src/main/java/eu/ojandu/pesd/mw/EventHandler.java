package eu.ojandu.pesd.mw;

import eu.ojandu.pesd.common.MessageHandler;
import eu.ojandu.pesd.common.dto.Event;
import eu.ojandu.pesd.common.entity.Character;
import eu.ojandu.pesd.common.repo.CharacterRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Random;

@Transactional
public class EventHandler extends MessageHandler<Event> {

    private static Logger log = LoggerFactory.getLogger(EventHandler.class);

    @Autowired
    private EventAggregator eventAggregator;

    @Autowired
    private CharacterRepo characterRepo;

    private Object lock = new Object();

    private char[] filterChars = new char[0];
    private int lessThan = Integer.MAX_VALUE;

    public EventHandler() {
        super(Event.class);
    }

    public void setFilterChars(char[] filterChars) {
        this.filterChars = filterChars;
    }

    public void setLessThan(int lessThan) {
        this.lessThan = lessThan;
    }

    private long counter = 0;
    private final long start = System.currentTimeMillis();
    private long idGen = new Random().nextInt(Integer.MAX_VALUE-200);

    @Override
    protected void handle(Event event) {
        counter++;
        long delta = System.currentTimeMillis() - start;
        if(delta != 0) {
            double coeff = 60_000.0 / delta;
            log.info("Event(avg {} per minute): {}", new Object[]{(int)(counter*coeff),event});
        }
        if(event.getValue() >= lessThan) {
            eventAggregator.filterEvent();
            return;
        }

        String name = event.getName();
        for(int i=0;i<name.length();i++) {
            for(int j=0;j<filterChars.length;j++) {
                if(name.charAt(i) == filterChars[j]) {
                    eventAggregator.filterEvent();
                    return;
                }
            }
        }

        for(int i=0;i<event.getName().length();i++) {
            increaseChar(event.getName().charAt(i));
        }

        eventAggregator.passEvent();
    }

    private void increaseChar(char c) {
        synchronized (lock) {
            Character character = characterRepo.getByValue("" + c);
            if(character == null) {
                character = new Character();
                character.setId(idGen++);
                character.setValue("" + c);
                character.setCount(0L);
            }
            character.setCount(character.getCount() + 1);
            characterRepo.save(character);
        }
    }
}
