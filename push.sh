#!/bin/bash

docker push vampnik/pesd-fe
docker push vampnik/pesd-mw
docker push vampnik/pesd-rest
docker push vampnik/pesd-stream-generator
docker push vampnik/pesd-logstash
